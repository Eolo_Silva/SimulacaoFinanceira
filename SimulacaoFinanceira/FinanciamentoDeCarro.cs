﻿namespace SimulacaoFinanceira
{
    using System;
    public class FinanciamentoDeCarro : Financiamento
    {
        private double TaxaAbertura = 0.0001;
        private double ValorResidual = 0.01;
        private double _duracaoEmprestimo;
        //No financiamento do carro, haverá sempre lugar a uma comissão de abertura de 0,01% e um valor residual de 1%. 
        //A duração do empréstimo não poderá ser superior a 60 meses.

        public FinanciamentoDeCarro(double montanteFinanciar, double prazoPagamento, double taxaJuro)
            : base(montanteFinanciar, prazoPagamento, taxaJuro)
        {
            _duracaoEmprestimo = prazoPagamento;
        }
        public double DuracaoEmprestimo
        {
            get
            {
                return _duracaoEmprestimo;
            }
            set
            {
                if (value >= 1 && value <= 60)
                {
                    _duracaoEmprestimo = value;
                }
                else
                {
                    _duracaoEmprestimo = 60;

                }
            }
        }



        public double ValorPrestacao()
        {
            return MontanteFinanciar * ValorResidual;
        }
        public override double CalculoFinanceiro()
        {
            double valorTaxa = MontanteFinanciar * TaxaJuro;
            double valorAbertura = MontanteFinanciar * TaxaAbertura;
            double valorResidual = MontanteFinanciar * ValorResidual;

            return ((MontanteFinanciar + valorTaxa + valorAbertura) - valorResidual) / _duracaoEmprestimo - 1;
        }
        public override string ToString()
        {
            return $" {base.ToString()}{Environment.NewLine} Prazo Pagamento :{DuracaoEmprestimo} Meses{Environment.NewLine} Comissão de Abertura: {TaxaAbertura * 100}% {Environment.NewLine} Valor Residual: {ValorResidual * 100}{Environment.NewLine} Prestação mensal dos primeiros {_duracaoEmprestimo - 1} Meses: {CalculoFinanceiro():C2}{Environment.NewLine} Ultima prestação: {ValorPrestacao():C2}";
        }
    }
}
