﻿namespace SimulacaoFinanceira
{
    using System;
    using System.Windows.Forms;
    public partial class Form1 : Form
    { 
        public Form1()
        {
            InitializeComponent();
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxMontanteFinanciar.Text) || !validacao(TextBoxMontanteFinanciar.Text))
            {
                MessageBox.Show("Insira um valor a financiar");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxPrazoPagamento.Text) || !validacao(TextBoxPrazoPagamento.Text))
            {
                MessageBox.Show("Insira uma taxa de financiamento");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxTaxasJuro.Text) || !validacao(TextBoxTaxasJuro.Text))
            {
                MessageBox.Show("Insira o prazo de pagamento");
                return;
            }

            if(ComboBoxFinanciamento.SelectedIndex == 0)
            {
                FinanciamentoDeCarro financiamentoDeCarro = new FinanciamentoDeCarro(
                Convert.ToDouble(TextBoxMontanteFinanciar.Text),
                Convert.ToDouble(TextBoxPrazoPagamento.Text),
                Convert.ToDouble(TextBoxTaxasJuro.Text) / 100);

                richTextBox1.Visible = true;
                richTextBox1.Text = financiamentoDeCarro.ToString();
            }
            else if (ComboBoxFinanciamento.SelectedIndex == 1)
            {
                if (string.IsNullOrEmpty(TextBoxNomeFiador.Text))
                {
                    MessageBox.Show("Insira o Nome do Fiador!");
                    return;
                }
                if (string.IsNullOrEmpty(TextBoxMoradaFiador.Text))
                {
                    MessageBox.Show("Insira a morada do Fiador!");
                    return;
                }
                if (string.IsNullOrEmpty(TextBoxTelefoneFiador.Text) || !validacao(TextBoxTelefoneFiador.Text))
                {
                    MessageBox.Show("Insira o telefone do Fiador!");
                    return;
                }
                if (string.IsNullOrEmpty(TextBoxNifFiador.Text) || !validacao(TextBoxNifFiador.Text))
                {
                    MessageBox.Show("Insira o NIF do Fiador!");
                    return;
                }
                if (string.IsNullOrEmpty(TextBoxRendimentoFiador.Text) || !validacao(TextBoxRendimentoFiador.Text))
                {
                    MessageBox.Show("Insira o rendimento bruto do Fiador!");
                    return;
                }
                FinanciamentoCasa financiamentoCasa = new FinanciamentoCasa(
                Convert.ToDouble(TextBoxMontanteFinanciar.Text),
                Convert.ToDouble(TextBoxPrazoPagamento.Text),
                Convert.ToDouble(TextBoxTaxasJuro.Text) / 100,
                TextBoxNomeFiador.Text,
                TextBoxMoradaFiador.Text,
                Convert.ToDouble(TextBoxTelefoneFiador.Text),
                Convert.ToDouble(TextBoxNifFiador.Text),
                Convert.ToDouble(TextBoxRendimentoFiador.Text));

                richTextBox1.Visible = true;
                richTextBox1.Text = financiamentoCasa.ToString();
   
            }
            else if (ComboBoxFinanciamento.SelectedIndex == 2)
            {
                OutrosFinanciamentos outrosFinanciamento = new OutrosFinanciamentos(
                Convert.ToDouble(TextBoxMontanteFinanciar.Text),
                Convert.ToDouble(TextBoxPrazoPagamento.Text),
                Convert.ToDouble(TextBoxTaxasJuro.Text) / 100);

                richTextBox1.Visible = true;
                richTextBox1.Text = outrosFinanciamento.ToString();
            }
        }

        private bool validacao(string text)
        {
            double num;
            if (double.TryParse(text, out num))
            {
                return true;
            }
            else
                return false;
        }
    }
}
