﻿using System;

namespace SimulacaoFinanceira
{
    public class FinanciamentoCasa : Financiamento
    {
        public double Spread = 0.015;
        public Fiador RendimentoAnual { get; set; }
        //No financiamento  da casa, haverá  lugar a  um spread  de  1,5%  e ao  registo de  um fiador
        //(nome, morada, telefone, nif e rendimento bruto anual). 
        public FinanciamentoCasa(double montanteFinanciar, double prazoPagamento, double taxaJuro, string nomes, string morada, double telefone, double nif, double rendimentoanual)
            : base(montanteFinanciar, prazoPagamento, taxaJuro)
        {
            RendimentoAnual = new Fiador(nomes, morada, telefone, nif, rendimentoanual);
        }
       
        public override double CalculoFinanceiro()
        {
            var ValorMensal = (MontanteFinanciar * TaxaJuro) + MontanteFinanciar / PrazoPagamento;
            return ValorMensal * Spread;
        }
        public override string ToString()
        {
            return $"{base.ToString()}{Environment.NewLine} Spread : {Spread * 100}{Environment.NewLine} Prestação : {CalculoFinanceiro()}{Environment.NewLine} Rendimento Bruto : {RendimentoAnual}";
        }
    }
}
