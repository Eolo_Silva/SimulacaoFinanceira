﻿namespace SimulacaoFinanceira
{
    using System;
    public abstract class Financiamento
    {
        public double MontanteFinanciar { get; set; }
        public double PrazoPagamento { get; set; }
        public double TaxaJuro { get; set; }

        public Financiamento(double montanteFinanciar, double prazoPagamento, double taxaJuro)
        {
            MontanteFinanciar = montanteFinanciar;
            PrazoPagamento = prazoPagamento;
            TaxaJuro = taxaJuro;
        }

        public override string ToString()
        {
            return $"{Environment.NewLine} Montante : {MontanteFinanciar}  {Environment.NewLine} Dias a Financiar : {PrazoPagamento}  {Environment.NewLine} Taxa de Juro : {TaxaJuro}";
        }

        public abstract double CalculoFinanceiro();
    }
}
