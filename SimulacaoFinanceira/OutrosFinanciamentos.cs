﻿namespace SimulacaoFinanceira
{
    using System;
    public class OutrosFinanciamentos : Financiamento
    {

        private double TaxaAbertura = 0.01;
        private double AmortizacaoAnticipada = 0.0005;


        //Finalmente nos  outros financiamentos, haverá  uma comissão  de abertura  de  1%  e uma  taxa 
        //de  amortização antecipada de 0,05%
        public OutrosFinanciamentos(double montanteFinanciar, double prazoPagamento, double taxaJuro)
            : base(montanteFinanciar, prazoPagamento, taxaJuro)
        {
            return;
        }

        public override double CalculoFinanceiro()
        {
            double ValorAbertura = (MontanteFinanciar * TaxaAbertura);
            double ValorAmortizacao = (MontanteFinanciar * AmortizacaoAnticipada);
            
            return (MontanteFinanciar * TaxaJuro) - ValorAmortizacao + ValorAbertura / PrazoPagamento;
        }
        public double CalculoAmortizacao()
        {
            return (MontanteFinanciar * AmortizacaoAnticipada);
        }
        public double CalculoAbertura()
        {
            return (MontanteFinanciar * TaxaAbertura);
        }
        public override string ToString()
        {
            return $" {base.ToString()}{Environment.NewLine} Valor Amortizacao: {CalculoAmortizacao()} Euros {Environment.NewLine} Valor da Abertura : {CalculoAbertura()} Euros{Environment.NewLine} Prestação Mensal : { CalculoFinanceiro():C2} Euros";
        }
    }
}
