﻿using System;

namespace SimulacaoFinanceira
{
    public class Fiador
    {
        private double mensalidade;
        public string Nomes;
        public string Morada;
        public double Telefone;
        public double Nif;
        public double RendimentoAnual { get; set; }

        public double Mensalidade
        {
            get
            {
                return mensalidade;
            }
            set
            {
                mensalidade = value;
            }
        }
        public Fiador(string nomes, string morada, double telefone, double nif, double rendimentoanual)
        {
            Nomes = nomes;
            Morada = morada;
            Telefone = telefone;
            Nif = nif;
            RendimentoAnual = rendimentoanual;
        }
        public override string ToString()
        {
            return $"Redimento do Fiador :{RendimentoAnual}";
        }
    }
}
