﻿namespace SimulacaoFinanceira
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxMontanteFinanciar = new System.Windows.Forms.TextBox();
            this.TextBoxTaxasJuro = new System.Windows.Forms.TextBox();
            this.TextBoxPrazoPagamento = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ComboBoxFinanciamento = new System.Windows.Forms.ComboBox();
            this.TextBoxNifFiador = new System.Windows.Forms.TextBox();
            this.TextBoxTelefoneFiador = new System.Windows.Forms.TextBox();
            this.TextBoxMoradaFiador = new System.Windows.Forms.TextBox();
            this.TextBoxNomeFiador = new System.Windows.Forms.TextBox();
            this.LabelNome = new System.Windows.Forms.Label();
            this.LabelMorada = new System.Windows.Forms.Label();
            this.LabelTelefone = new System.Windows.Forms.Label();
            this.LabelRendimento = new System.Windows.Forms.Label();
            this.LabelNif = new System.Windows.Forms.Label();
            this.TextBoxRendimentoFiador = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // TextBoxMontanteFinanciar
            // 
            this.TextBoxMontanteFinanciar.Location = new System.Drawing.Point(57, 58);
            this.TextBoxMontanteFinanciar.Name = "TextBoxMontanteFinanciar";
            this.TextBoxMontanteFinanciar.Size = new System.Drawing.Size(211, 20);
            this.TextBoxMontanteFinanciar.TabIndex = 0;
            // 
            // TextBoxTaxasJuro
            // 
            this.TextBoxTaxasJuro.Location = new System.Drawing.Point(57, 105);
            this.TextBoxTaxasJuro.Name = "TextBoxTaxasJuro";
            this.TextBoxTaxasJuro.Size = new System.Drawing.Size(211, 20);
            this.TextBoxTaxasJuro.TabIndex = 1;
            // 
            // TextBoxPrazoPagamento
            // 
            this.TextBoxPrazoPagamento.Location = new System.Drawing.Point(57, 154);
            this.TextBoxPrazoPagamento.Name = "TextBoxPrazoPagamento";
            this.TextBoxPrazoPagamento.Size = new System.Drawing.Size(211, 20);
            this.TextBoxPrazoPagamento.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Montante à Financiar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Taxa de Juros";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(114, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Prazo de Pagamento";
            // 
            // ComboBoxFinanciamento
            // 
            this.ComboBoxFinanciamento.FormattingEnabled = true;
            this.ComboBoxFinanciamento.Items.AddRange(new object[] {
            "Carro",
            "Casa",
            "Outros"});
            this.ComboBoxFinanciamento.Location = new System.Drawing.Point(605, 145);
            this.ComboBoxFinanciamento.Name = "ComboBoxFinanciamento";
            this.ComboBoxFinanciamento.Size = new System.Drawing.Size(134, 21);
            this.ComboBoxFinanciamento.TabIndex = 14;
            // 
            // TextBoxNifFiador
            // 
            this.TextBoxNifFiador.Location = new System.Drawing.Point(605, 301);
            this.TextBoxNifFiador.Name = "TextBoxNifFiador";
            this.TextBoxNifFiador.Size = new System.Drawing.Size(135, 20);
            this.TextBoxNifFiador.TabIndex = 18;
            // 
            // TextBoxTelefoneFiador
            // 
            this.TextBoxTelefoneFiador.Location = new System.Drawing.Point(605, 274);
            this.TextBoxTelefoneFiador.Name = "TextBoxTelefoneFiador";
            this.TextBoxTelefoneFiador.Size = new System.Drawing.Size(135, 20);
            this.TextBoxTelefoneFiador.TabIndex = 17;
            // 
            // TextBoxMoradaFiador
            // 
            this.TextBoxMoradaFiador.Location = new System.Drawing.Point(605, 248);
            this.TextBoxMoradaFiador.Name = "TextBoxMoradaFiador";
            this.TextBoxMoradaFiador.Size = new System.Drawing.Size(135, 20);
            this.TextBoxMoradaFiador.TabIndex = 16;
            // 
            // TextBoxNomeFiador
            // 
            this.TextBoxNomeFiador.Location = new System.Drawing.Point(605, 222);
            this.TextBoxNomeFiador.Name = "TextBoxNomeFiador";
            this.TextBoxNomeFiador.Size = new System.Drawing.Size(135, 20);
            this.TextBoxNomeFiador.TabIndex = 15;
            // 
            // LabelNome
            // 
            this.LabelNome.AutoSize = true;
            this.LabelNome.Location = new System.Drawing.Point(513, 225);
            this.LabelNome.Name = "LabelNome";
            this.LabelNome.Size = new System.Drawing.Size(88, 13);
            this.LabelNome.TabIndex = 19;
            this.LabelNome.Text = "Nome Completo :";
            // 
            // LabelMorada
            // 
            this.LabelMorada.AutoSize = true;
            this.LabelMorada.Location = new System.Drawing.Point(552, 252);
            this.LabelMorada.Name = "LabelMorada";
            this.LabelMorada.Size = new System.Drawing.Size(49, 13);
            this.LabelMorada.TabIndex = 20;
            this.LabelMorada.Text = "Morada :";
            // 
            // LabelTelefone
            // 
            this.LabelTelefone.AutoSize = true;
            this.LabelTelefone.Location = new System.Drawing.Point(546, 278);
            this.LabelTelefone.Name = "LabelTelefone";
            this.LabelTelefone.Size = new System.Drawing.Size(55, 13);
            this.LabelTelefone.TabIndex = 21;
            this.LabelTelefone.Text = "Telefone :";
            // 
            // LabelRendimento
            // 
            this.LabelRendimento.AutoSize = true;
            this.LabelRendimento.Location = new System.Drawing.Point(472, 330);
            this.LabelRendimento.Name = "LabelRendimento";
            this.LabelRendimento.Size = new System.Drawing.Size(128, 13);
            this.LabelRendimento.TabIndex = 22;
            this.LabelRendimento.Text = "Rendimento Bruto Anual :";
            // 
            // LabelNif
            // 
            this.LabelNif.AutoSize = true;
            this.LabelNif.Location = new System.Drawing.Point(570, 305);
            this.LabelNif.Name = "LabelNif";
            this.LabelNif.Size = new System.Drawing.Size(30, 13);
            this.LabelNif.TabIndex = 23;
            this.LabelNif.Text = "NIF :";
            // 
            // TextBoxRendimentoFiador
            // 
            this.TextBoxRendimentoFiador.Location = new System.Drawing.Point(605, 327);
            this.TextBoxRendimentoFiador.Name = "TextBoxRendimentoFiador";
            this.TextBoxRendimentoFiador.Size = new System.Drawing.Size(135, 20);
            this.TextBoxRendimentoFiador.TabIndex = 24;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::SimulacaoFinanceira.Properties.Resources.DSDGS;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(467, 93);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 73);
            this.button1.TabIndex = 25;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(470, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "CALCULAR";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(56, 196);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(324, 178);
            this.richTextBox1.TabIndex = 31;
            this.richTextBox1.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SimulacaoFinanceira.Properties.Resources.Credito;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(849, 421);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextBoxRendimentoFiador);
            this.Controls.Add(this.LabelNif);
            this.Controls.Add(this.LabelRendimento);
            this.Controls.Add(this.LabelTelefone);
            this.Controls.Add(this.LabelMorada);
            this.Controls.Add(this.LabelNome);
            this.Controls.Add(this.TextBoxNifFiador);
            this.Controls.Add(this.TextBoxTelefoneFiador);
            this.Controls.Add(this.TextBoxMoradaFiador);
            this.Controls.Add(this.TextBoxNomeFiador);
            this.Controls.Add(this.ComboBoxFinanciamento);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxPrazoPagamento);
            this.Controls.Add(this.TextBoxTaxasJuro);
            this.Controls.Add(this.TextBoxMontanteFinanciar);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxMontanteFinanciar;
        private System.Windows.Forms.TextBox TextBoxTaxasJuro;
        private System.Windows.Forms.TextBox TextBoxPrazoPagamento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ComboBoxFinanciamento;
        private System.Windows.Forms.TextBox TextBoxNifFiador;
        private System.Windows.Forms.TextBox TextBoxTelefoneFiador;
        private System.Windows.Forms.TextBox TextBoxMoradaFiador;
        private System.Windows.Forms.TextBox TextBoxNomeFiador;
        private System.Windows.Forms.Label LabelNome;
        private System.Windows.Forms.Label LabelMorada;
        private System.Windows.Forms.Label LabelTelefone;
        private System.Windows.Forms.Label LabelRendimento;
        private System.Windows.Forms.Label LabelNif;
        private System.Windows.Forms.TextBox TextBoxRendimentoFiador;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

